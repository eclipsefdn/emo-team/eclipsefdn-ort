
# eclipsefdn-ort

This repository holds information about Eclipse's ORT setup and deployment.

More information can be found at:

* The [project repository itself](https://gitlab.eclipse.org/eclipsefdn/emo-team/eclipsefdn-ort)
  - We use the [issues there](https://gitlab.eclipse.org/eclipsefdn/emo-team/eclipsefdn-ort/-/issues) to track requirements, progress and bugs.
  - The `main` branch is the stable, deployed version whereas `dev` is the on-going development branch.
  - Documentation is [available in the wiki](https://gitlab.eclipse.org/eclipsefdn/emo-team/eclipsefdn-ort/-/wikis/home).
* The static web site found on the VM's standard port: \
  https://ort-vm1.eclipse.org
  
For any question or inquiry, please submit an issue or get in touch with Boris.
  

## Structure of the repository

```
$ tree
.
├── conf                            # All configuration files needed
│   ├── ...                         # to execute ORT with our reqs
│   └── config.yml
├── README.md
└── scripts                         # Scripts to automate runs and
    ├── ...                         # conf as much as possible
    └── run_ort.sh
```

The repository is self-contained, everything required to run ORT with our
own configuration and setup should be included.

## Setup

We provide 3 scripts:
* `setup_ort_conf.sh` creates the proper configuration files for licenses classification and curations from IPZilla. Only needed when there is a change in the configuration files or for a new setup.
* `run_ort.sh` runs the ORT analysis on a specific project.
* `run_extract.sh` parses the log files of all runs and publishes a static website with all projects, runs and errors. It then copies the published website into `/var/www/html/` for direct access through the host's web serving Apache.

Check and edit accordingly the environment variables in the script `run_ort.sh` to point to the
actual location of the repository and binaries (ORT, ScanCode) and execute
the script:

```
$ time bash run_ort.sh /path/to/project/ /path/to/results/
```

Execution time varies from a few minutes to a few hours, depending on the
size of the project.

**Learn more about execution in the dedicated document: [Executing ORT](https://gitlab.eclipse.org/eclipsefdn/emo-team/eclipsefdn-ort/-/wikis/Executing-ORT).**

## Branches

There are main 3 branches defined in the repository:
* `main` is what is deployed on the main production server.
* `staging` is what is deployed on the staging server.
* `dev` serves as a temporary space where differences between prod and staging can be made. Namely, the only difference between the dev and staging branches after a synchronisation is in the `Jenkinsfile_*` to use the proper nodes and jobs.

