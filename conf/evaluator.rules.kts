import org.apache.logging.log4j.kotlin.Logging

object GlobalLogger : Logging

val approvedLicenses = licenseClassifications.licensesByCategory["eclipse:approved"].orEmpty()
val restrictedLicenses = licenseClassifications.licensesByCategory["eclipse:restricted"].orEmpty()

val ruleSet = ruleSet(ortResult, licenseInfoResolver) {
    val licenseView = LicenseView.CONCLUDED_OR_DECLARED_AND_DETECTED

    GlobalLogger.logger.info {
        "There are ${approvedLicenses.size} approved and ${restrictedLicenses.size} restricted licenses."
    }

    packageRule("PROJECT_CHECK") {
        // Requirements for the rule to trigger a violation.
        require {
            -isExcluded()
            +isProject()
        }

        // Define a rule that is executed for each license of the project.
        licenseRule("PROJECT_LICENSE_CHECK", licenseView) {
            val projCoords = pkg.metadata.id.toCoordinates()

            when (license) {
                in approvedLicenses -> GlobalLogger.logger.debug { "$license is an approved license." }

                in restrictedLicenses -> error(
                    message = "License $license of project '$projCoords' is classified as restricted.",
                    howToFix = "Do not use $license as a license for project '$projCoords'."
                )

                else -> hint(
                    message = "License $license of project '$projCoords' is unclassified.",
                    howToFix = "Classify $license as either approved or restricted."
                )
            }
        }
    }

    // Define a rule that is executed for each dependency.
    dependencyRule("DEPENDENCY_CHECK") {
        // Requirements for the rule to trigger a violation.
        require {
            -isExcluded()
        }

        // Define a rule that is executed for each license of the dependency.
        licenseRule("DEPENDENCY_LICENSE_CHECK", licenseView) {
            val depCoords = pkg.metadata.id.toCoordinates()

            when (license) {
                in approvedLicenses -> GlobalLogger.logger.debug { "$license is an approved license." }

                in restrictedLicenses -> error(
                    message = "License $license of dependency '$depCoords' is classified as restricted.",
                    howToFix = "Remove dependency '$depCoords' from project '${project.id.toCoordinates()}'."
                )

                else -> hint(
                    message = "License $license of dependency '$depCoords' is unclassified.",
                    howToFix = "Classify $license as either approved or restricted."
                )
            }
        }
    }
}

// Populate the list of rule violations to return.
ruleViolations += ruleSet.violations
