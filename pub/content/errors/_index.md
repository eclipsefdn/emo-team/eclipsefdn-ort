---
title: "Errors"
type: "list"
---

This is the list of violations (aka errors) encountered on all **last runs of projects**.

The full list of violations across all projects can be downloaded as a CSV file from [/violations.csv](/violations.csv).

Each project and run has a violations.csv file as well that can be downloaded from their own page.

-----


