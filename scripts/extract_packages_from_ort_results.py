
import json
import sys
import re

file_in = sys.argv[1]

if not file_in:
    print('Need a file to work on.')
    print(f'Usage: {sys.argv[0]} results.json')
    exit()

#print(f"Reading results from {file_in}.")
    
with open(file_in, 'r') as f:
    results_f=f.read()

# parse file
results = json.loads(results_f)

npm_id_re = re.compile('^NPM:(?P<namespace>[^:]*):(?P<name>[^:]*):(?P<version>[^:]*)$')
#print(f"Found {len(results['analyzer']['result']['packages'])} packages.")
for i in results['analyzer']['result']['packages']:
#    print(i['package']['id'])
    npm_id_match = npm_id_re.match(i['package']['id'])
    ns = npm_id_match.group('namespace') if npm_id_match.group('namespace') else '-'
    print(f"npm/npmjs/{ns}/{npm_id_match.group('name')}/{npm_id_match.group('version')}")
