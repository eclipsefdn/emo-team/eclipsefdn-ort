#!/bin/bash

###########################################################################
# Copyright (c) 2022 The Eclipse Foundation.
#
# This program and the accompanying materials are made available under
# the terms of the Eclipse Public License 2.0 which accompanies this
# distribution, and is available at https://www.eclipse.org/legal/epl-2.0
#
# SPDX-License-Identifier: EPL-2.0
###########################################################################

# A script to get or update all Eclipse repositories, as listed in
# www.eclipse.org/projects/export/repositories.csv.php

DIR_PROJECTS="/data/projects"


# Get the csv file from the API.
echo "# Downloading repositories.csv"
wget --quiet https://www.eclipse.org/projects/export/repositories.csv.php -O /tmp/repositories.csv

OLD_PWD=$(pwd)

# Make sure we don't get stuck by a password prompt.
export GIT_TERMINAL_PROMPT=0

# Loop through all lines BUT the first one.
for i in `cat /tmp/repositories.csv | grep -v '^#'`; do
    p=$(echo $i | cut -d, -f1);
    r=$(echo $i | cut -d, -f2);
    n=${r##*/}

    # Create project dir if needed.
    DIR_PROJECT=${DIR_PROJECTS}/$p
    mkdir -p ${DIR_PROJECT}
    cd ${DIR_PROJECT}

    # Check if project is already cloned.
    # If no, clone it, otherwise just update it.
    if [[ -d $n ]]; then
	echo "# Cloning $p/$n";
	cd $n
	# Remove local changes (due to analysis/builds)
	git reset --hard
	git config pull.rebase false
	git pull --force --prune
    else
	echo "# Pulling $p/$n";
	git clone $r $n
    fi
    
done

cd $OLD_PWD
rm /tmp/repositories.csv
