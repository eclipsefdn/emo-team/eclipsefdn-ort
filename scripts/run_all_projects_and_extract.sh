
echo "Execute all projects in /data/projects/"
for p in /data/projects/*; do
    for r in `ls ${p}/`; do
        echo "Run analysis on $p/$r"
        time bash /ort/eclipsefdn-ort/scripts/run_ort.sh $p $r /data/results/;
        echo "Run extract"
        bash /ort/eclipsefdn-ort/scripts/run_extract.sh
    done
done



