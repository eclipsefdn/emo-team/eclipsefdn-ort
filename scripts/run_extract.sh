#!/bin/bash

###########################################################################
# Copyright (c) 2022 The Eclipse Foundation.
#
# This program and the accompanying materials are made available under
# the terms of the Eclipse Public License 2.0 which accompanies this
# distribution, and is available at https://www.eclipse.org/legal/epl-2.0
#
# SPDX-License-Identifier: EPL-2.0
###########################################################################

DIR_RESULTS=/data/results
DIR_FDN_ORT=/ort/eclipsefdn-ort
DIR_WWW=/var/www/html


## Don't edit below (or know what you're doing).

DIR_PUBLISH=${DIR_FDN_ORT}/pub

echo "Generate markdown files from results."
rm -rf ${DIR_PUBLISH}/content/projects/*/
rm -rf ${DIR_PUBLISH}/content/runs/*/
rm -rf ${DIR_PUBLISH}/content/errors/*/
python3 ${DIR_FDN_ORT}/scripts/utils/extract_info.py -r $DIR_RESULTS -o ${DIR_PUBLISH}/content/

echo "Generate hugo static web site."
rm -rf ${DIR_PUBLISH}/public/*
cd ${DIR_PUBLISH}
hugo
cd -

echo "Copy to http-served directory."
rm -rf ${DIR_WWW}/*
cp -r ${DIR_PUBLISH}/public/* ${DIR_WWW}/

echo "Creating symlink from ${DIR_WWW}/files to $DIR_RESULTS."
if [[ -L ${DIR_WWW}/files ]]; then
   rm ${DIR_WWW}/files
fi
ln -s $DIR_RESULTS ${DIR_WWW}/files

