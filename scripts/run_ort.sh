#!/bin/bash

###########################################################################
# Copyright (c) 2022 The Eclipse Foundation.
#
# This program and the accompanying materials are made available under
# the terms of the Eclipse Public License 2.0 which accompanies this
# distribution, and is available at https://www.eclipse.org/legal/epl-2.0
#
# SPDX-License-Identifier: EPL-2.0
###########################################################################

# Get the full path of the scripts/ directory.
ORT_ECLIPSE_SCRIPTS=$(dirname $(readlink -f "$0"))

export ORT_CONFIG_DIR=/ort/eclipsefdn-ort/conf
# Note: This one is set in the conf file.
#export ORT_DATA_DIR=/data/cache/ort/
export ORT_DATA_DIR=/data/cache/ort/
#export GRADLE_USER_HOME=/data/cache/gradle/
export DOCKER_DATA_DIR=/ort_cache/
export DOCKER_CONFIG_DIR=/home/ort/.ort/config/

# Docker param vars
export DOCKER_IMAGE=ort
export ORT_ENV_FILE=/ort/eclipsefdn-ort/run_env.txt

# Check parameters
die () {
    echo >&2 "$@"
    echo "Usage: $0 <dir_project> <repo> <dir_results>"
    exit 1
}
[ "$#" -eq 3 ] || die "3 arguments required, $# provided."

PROJECT_IN=$1
if [ ! -d $PROJECT_IN ]; then
    echo "[$PROJECT_IN] is not a directory. Quitting."
    exit 0
fi
    
REPO_IN=$2
if [ ! -d $PROJECT_IN/$REPO_IN ]; then
    echo "[$REPO_IN] is not a directory. Quitting."
    exit 0
fi

DIR_IN=${PROJECT_IN}/${REPO_IN}

DIR_OUT_BASE=$3
[ -d $DIR_OUT_BASE ] || die "[$DIR_OUT_BASE] is not a directory."


# Compute directory vars
PROJECT=$(basename $PROJECT_IN)
DATE_RUN=$(date +'%Y%m%d%H%M')
DIR_OUT="${DIR_OUT_BASE%/}/${PROJECT%/}/${REPO_IN%/}/"
LOG="${DIR_OUT_BASE%/}/${PROJECT%/}/${REPO_IN%/}/${PROJECT%/}_${REPO_IN%/}_${DATE_RUN}.log"
mkdir -p "${DIR_OUT}"

# Check if env vars are set, otherwise optionally set defaults.
if [[ -z "$ORT_CONFIG_DIR" ]]; then
    echo "Please set ORT_CONFIG_DIR env var to point to the conf directory."
    die "Cannot find ORT_CONFIG_DIR directory."
fi

if [[ -z "$JAVA_HOME" ]]; then
    # This is the default on debian
    export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
fi


# Use for debug purpose - these should be normally defined in the PATH.
#export PATH=/opt/ort/cli/build/install/ort/bin/:$PATH
#export PATH=/opt/scancode-toolkit-30.1.0/:$PATH

echo "Start scan at $(date +"%Y-%m-%d %H:%M")" | tee -a $LOG
echo "- Using docker ort image [${DOCKER_IMAGE}]." | tee -a $LOG
echo "- Log written to [$LOG]." | tee -a $LOG
echo "- Scripts dir [${ORT_ECLIPSE_SCRIPTS}]." | tee -a $LOG
echo "- Working on repository [${DIR_IN}]." | tee -a $LOG
echo "" | tee -a $LOG

# Check if the project already provides a custom .ort.yml file
echo "* Checking if we can find a project specific ort conf." | tee -a $LOG
echo "  - Trying [${DIR_IN}/.ort.yml]." | tee -a $LOG
if [[ -f "${DIR_IN}/.ort.yml" ]]; then
    # If yes, then use it.
    ORT_REPO_CONF=/project/.ort.yml
    IS_ORT_CONF_LOCAL=0
    echo "  - Using repo conf file from repo at [${ORT_REPO_CONF}]." | tee -a $LOG
else 
    # If not, then create or reuse a local one.
    ORT_REPO_CONF_DIR=${ORT_CONFIG_DIR}/repositories/${PROJECT}/${REPO_IN}/
    if [[ ! -f "${ORT_REPO_CONF_DIR}/ort.yml" ]]; then
        mkdir -p ${ORT_REPO_CONF_DIR}
        echo "  - No default local ort.yml file found, creating one from template." | tee -a $LOG
        cp ${ORT_CONFIG_DIR}/repositories/ort_template.yml ${ORT_REPO_CONF_DIR}/ort.yml
    else
        echo "  - Found [${ORT_REPO_CONF_DIR}/ort.yml]. Using it for next steps." | tee -a $LOG
    fi
    ORT_REPO_CONF=${DOCKER_CONFIG_DIR}/repositories/$PROJECT/$REPO_IN/ort.yml
    IS_ORT_CONF_LOCAL=1
    echo "  - Using repo conf file from local at [${ORT_REPO_CONF}]." | tee -a $LOG
fi
echo "" | tee -a $LOG

echo "* Checking if results files already exist in output." | tee -a $LOG
if [[ -f "${DIR_OUT}/analyzer-result.json" ]]; then
    echo "  - Result files already exist. Wiping them out." | tee -a $LOG
    rm "${DIR_OUT}"/*.json
else
    echo "  - None found, proceeding." | tee -a $LOG
fi


# Run the analyzer.
echo "* Start analyzer at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo docker run -v ${DIR_IN}:/project \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} \
    -v ${DIR_OUT}:/output --rm \
    -e JAVA_OPTS="-Xmx15g" \
    ${DOCKER_IMAGE} --info analyze -f JSON  \
    --repository-configuration-file ${DOCKER_CONFIG_DIR}/repositories/$PROJECT/$REPO_IN/ort.yml \
    -i /project -o /output | tee -a $LOG
#    --package-curations-dir ${DOCKER_CONFIG_DIR}/curations/ \
time docker run -v ${DIR_IN}:/project \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} \
    -v ${DIR_OUT}:/output --rm \
    -e JAVA_OPTS="-Xmx15g" \
    ${DOCKER_IMAGE} --info analyze -f JSON  \
    --repository-configuration-file ${ORT_REPO_CONF} \
    -i /project -o /output | tee -a $LOG
#    --package-curations-dir ${DOCKER_CONFIG_DIR}/curations/ \
echo "* Finish analyzer at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo "" | tee -a $LOG


# Update ort.yml file with the analyzer results if it's local.
if [ "IS_ORT_CONF_LOCAL" == 1 ]; then
    echo "* Updating ort.yml file at [${ORT_REPO_CONF}]."
    orth repository-configuration generate-scope-excludes \
         -i ${DIR_OUT}/analyzer-result.json \
         --repository-configuration-file ${ORT_CONFIG_DIR}/repositories/${PROJECT}/${REPO_IN}/ort.yml
    echo "" | tee -a $LOG
else
    echo "* Found project-specific ort conf. Not updating the project .ort.yml file."
fi


# Run the scanner.
echo "* Start scanner at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo docker run -v ${DIR_OUT}:/output \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} --rm \
    -e JAVA_OPTS="-Xmx15g" \
    --env-file /ort/eclipsefdn-ort/run_env.txt \
    ${DOCKER_IMAGE} --info scan -f JSON \
    --scanners ScanCode \
    -i /output/analyzer-result.json -o /output | tee -a $LOG
time docker run -v ${DIR_OUT}:/output \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} --rm \
    -e JAVA_OPTS="-Xmx15g" \
    --env-file ${ORT_ENV_FILE} \
    ${DOCKER_IMAGE} --info scan -f JSON \
    --scanners ScanCode \
    -i /output/analyzer-result.json -o /output | tee -a $LOG
echo "* Finish scanner at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo "" | tee -a $LOG


# Run the advisor.
echo "* Start advisor at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo docker run -v ${DIR_OUT}:/output \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} --rm \
    -e JAVA_OPTS="-Xmx15g" \
     ${DOCKER_IMAGE} --info advise -f JSON \
     --advisors OssIndex,OSV \
     -i /output/scan-result.json -o /output | tee -a $LOG
time docker run -v ${DIR_OUT}:/output \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} --rm \
    -e JAVA_OPTS="-Xmx15g" \
     ${DOCKER_IMAGE} --info advise -f JSON \
     --advisors OssIndex,OSV \
     -i /output/scan-result.json -o /output | tee -a $LOG
echo "* Finish advisor at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo "" | tee -a $LOG


# Run the evaluator.
echo "* Start evaluator at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo docker run -v ${DIR_OUT}:/output \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} --rm \
    -e JAVA_OPTS="-Xmx15g" \
     ${DOCKER_IMAGE} --info evaluate -f JSON \
    --license-classifications-file ${DOCKER_CONFIG_DIR}/license-classifications.yml \
    --package-curations-dir ${DOCKER_CONFIG_DIR}/curations/ \
    --rules-file ${DOCKER_CONFIG_DIR}/evaluator.rules.kts \
    --rules-resource /rules/osadl.rules.kts \
    --repository-configuration-file ${DOCKER_CONFIG_DIR}/repositories/$PROJECT/$REPO_IN/ort.yml \
    -i /output/advisor-result.json -o /output | tee -a $LOG
time docker run -v ${DIR_OUT}:/output \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} --rm \
    -e JAVA_OPTS="-Xmx15g" \
     ${DOCKER_IMAGE} --info evaluate -f JSON \
    --license-classifications-file ${DOCKER_CONFIG_DIR}/license-classifications.yml \
    --package-curations-dir ${DOCKER_CONFIG_DIR}/curations/ \
    --rules-file ${DOCKER_CONFIG_DIR}/evaluator.rules.kts \
    --rules-resource /rules/osadl.rules.kts \
    --repository-configuration-file ${DOCKER_CONFIG_DIR}/repositories/$PROJECT/$REPO_IN/ort.yml \
    -i /output/advisor-result.json -o /output | tee -a $LOG
echo "* Finish evaluator at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo "" | tee -a $LOG


# Run the reporter.
echo "* Start reporter at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo docker run -v ${DIR_OUT}:/output \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} --rm \
    -e JAVA_OPTS="-Xmx15g" \
    ${DOCKER_IMAGE} --info report \
    --license-classifications-file ${DOCKER_CONFIG_DIR}/license-classifications.yml \
    -f NoticeTemplate,SpdxDocument,CycloneDx,StaticHtml,WebApp \
    --repository-configuration-file ${DOCKER_CONFIG_DIR}/repositories/$PROJECT/$REPO_IN/ort.yml \
    -i /output/evaluation-result.json -o /output | tee -a $LOG
time docker run -v ${DIR_OUT}:/output \
    -v ${ORT_DATA_DIR}:${DOCKER_DATA_DIR} \
    -v ${ORT_CONFIG_DIR}:${DOCKER_CONFIG_DIR} --rm \
    -e JAVA_OPTS="-Xmx15g" \
    ${DOCKER_IMAGE} --info report \
    --license-classifications-file ${DOCKER_CONFIG_DIR}/license-classifications.yml \
    -f NoticeTemplate,SpdxDocument,CycloneDx,StaticHtml,WebApp \
    --repository-configuration-file ${DOCKER_CONFIG_DIR}/repositories/$PROJECT/$REPO_IN/ort.yml \
    -i /output/evaluation-result.json -o /output | tee -a $LOG
echo "* Finish reporter at $(date +'%Y-%m-%d %H:%M')" | tee -a $LOG
echo "" | tee -a $LOG


# Clean up /tmp directory, remove all directories at level 1
# that belong to us and that have not been modified for at least 12 hours
find /tmp/ -maxdepth 1 -user $USER -type d -mmin +720 -exec rm -rf {} \;

# Parse JSON evaluation report, get clearlydefined data and create curations file.
#python3 ${ORT_ECLIPSE_SCRIPTS}/utils/curate_clearlydefined.py -i $DIR_OUT/evaluation-result.json -o ${ORT_CONFIG_DIR}/curations/clearlydefined_curations.yml

