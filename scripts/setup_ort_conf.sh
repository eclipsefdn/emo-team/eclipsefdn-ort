#!/bin/bash

###########################################################################
# Copyright (c) 2022 The Eclipse Foundation.
#
# This program and the accompanying materials are made available under
# the terms of the Eclipse Public License 2.0 which accompanies this
# distribution, and is available at https://www.eclipse.org/legal/epl-2.0
#
# SPDX-License-Identifier: EPL-2.0
###########################################################################

ORT_HOME=/ort/ort
ECL_ORT_HOME=/ort/eclipsefdn-ort/

echo "Setting up configuration files for ORT @ Eclipse."

# Check that ORT_HOME and ECL_ORT_HOME exist.
if [ ! -d ${ORT_HOME} ]; then
    echo "Cannot find ORT_HOME at ${ORT_HOME}."
    echo "Exiting."
    exit 1
fi

if [ ! -d ${ECL_ORT_HOME} ]; then
    echo "Cannot find ECL_ORT_HOME at ${ECL_ORT_HOME}."
    echo "Exiting."
    exit 1
fi

#echo "Downloading Eclipse licenses list.."
#wget --quiet https://www.eclipse.org/legal/licenses.json -O ../conf/eclipse_licenses_approved.json 

# Generate license classifications for Eclipse setup
cd ${ORT_HOME}
./gradlew :helper-cli:run --args="license-classifications import eclipse -p eclipse: -o eclipse.yml"
mv ${ORT_HOME}/helper-cli/eclipse.yml ${ECL_ORT_HOME}/conf/license-classifications.yml

echo "Downloading Eclipse curations list from IPLab.."
wget --quiet http://www.eclipse.org/projects/services/curations.yml.php -O curations.yml.php
python3 ${ECL_ORT_HOME}/scripts/utils/get_curations.py curations.yml.php

mkdir -p ${ECL_ORT_HOME}/conf/curations/
mv curations.yml ${ECL_ORT_HOME}/conf/curations/eclipse_ip_curations.yml
rm curations.yml.php


