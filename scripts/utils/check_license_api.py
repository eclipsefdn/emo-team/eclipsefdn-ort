###########################################################################
# Copyright (c) 2022 The Eclipse Foundation.
#
# This program and the accompanying materials are made available under
# the terms of the Eclipse Public License 2.0 which accompanies this
# distribution, and is available at https://www.eclipse.org/legal/epl-2.0
#
# SPDX-License-Identifier: EPL-2.0
###########################################################################

# This script checks the curated information of a specific package
# against the Eclipse Foundation's ip knowledge base.

import sys
import requests, json

if len(sys.argv) > 3:
    system = sys.argv[1]
    package = sys.argv[2]
    version = sys.argv[3]
else:
    print(f"Usage:   {sys.argv[0]} system package version")
    print(f"Example: {sys.argv[0]} npm/npmjs seek-bzip 1.0.5")
    exit()

query = f'{system}/-/{package}/{version}'

ipzilla_url = "http://www.eclipse.org/projects/services/license_check.php"
payload = {'content': query}
ret = json.loads(
    requests.post(ipzilla_url, data=payload).text
)

if 'approved' in ret.keys() and query in ret['approved']:
    license = ret['approved'][query]['license']
    status = ret['approved'][query]['status']
    authority = ret['approved'][query]['authority']
    confidence = ret['approved'][query]['confidence']
    print(f"OK: package [{package}:{version}] has license [{license}] approved by [{authority}] with conf [{confidence}].")
else:
    print(f"FAILED: \n{ret}.")

          

