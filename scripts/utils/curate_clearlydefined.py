###########################################################################
# Copyright (c) 2022 The Eclipse Foundation.
#
# This program and the accompanying materials are made available under
# the terms of the Eclipse Public License 2.0 which accompanies this
# distribution, and is available at https://www.eclipse.org/legal/epl-2.0
#
# SPDX-License-Identifier: EPL-2.0
###########################################################################

# This script takes a ORT evaluator JSON output file and looks for
# the packages on clearlydefined.io. If the package is defined there, then
# build a set of curations with the proper concluded license.

import json
import requests
import getopt
import sys
import yaml
from datetime import datetime
from os.path import isfile


# Parse command line arguments
try:
    arguments, values = getopt.getopt(sys.argv[1:], "i:o:", ["input=", "output="])
except getopt.error as err:
    # Output error, and return with an error code
    print(str(err))
    sys.exit(2)

file_in = ''
file_out = ''

if len(arguments) < 2:
    exit("Usage: curate clearlydefined.py -i evaluation-result.json -o mycurations.yml")

# Evaluate given options
for current_argument, current_value in arguments:
    if current_argument in ("-i", "--input"):
        file_in = current_value
    elif current_argument in ("-o", "--output"):
        file_out = current_value

# Read JSON result file from ORT
print(f"Reading JSON file from {file_in}.")
with open(file_in, 'r') as f:
    try:
        doc = json.load(f)
    except json.JSONDecodeError as e:
        print(f"Could not parse JSON from {file_in}: error {e}")
        exit()

packages = doc['evaluator']['violations']
# Make the list of packages unique based on 'id'
packages_uniq = {}
for package in packages:
    packages_uniq[package['pkg']] = package
packages = packages_uniq

licenses = {}
urls={}

# Loop through all violations IDs
for package_id in packages:
    if not package_id:
        continue
    package = packages[package_id]
    pkg = package['pkg']
    print(f"- Working on '{package_id}' '{package}' '{pkg}'..", end=" ")
    # Transform coordinates
    parts = pkg.split(':')
    url = 'https://api.clearlydefined.io/definitions/'
    if pkg.startswith('NPM:'):
        coordinates = f"npm/npmjs/-/{parts[-2]}/{parts[-1]}"
        url += coordinates
    elif pkg.startswith('Maven:'):
        namespace = parts[-3] if (parts[-3] != '') else '-'
        coordinates = f"maven/mavencentral/{namespace}/{parts[-2]}/{parts[-1]}"
        url += coordinates
    else:
        print("Package type not recognised.")
        continue
    
    # Do a get request to clearlydefined, as described in
    # https://docs.clearlydefined.io/using-data
    r = requests.get(url)
    if r.status_code == 200:
        answer = r.json()
        if 'licensed' in answer and 'declared' in answer['licensed']:
            print(f"Found license: {answer['licensed']['declared']}.", end=" ")
            if 'score' in answer['licensed'] and answer['licensed']['score']['total'] >= 60:
                print(f"Total license score is {answer['licensed']['score']['total']}, accepted.")
                licenses[pkg] = answer['licensed']['declared']
                urls[pkg] = 'https://clearlydefined.io/definitions/' + coordinates
            else:
                print(f"Total license score is {answer['licensed']['score']['total']}, NOT accepted.")
        else:
            print("No declared license found on clearlydefined.")
    else:
        print("Not found on clearlydefined (status_code).")

print(f"\nFound {len(licenses)} curations.")


# Now update the yaml curations file

curations_in = []
ids = []
if isfile(file_out):
    print(f"Read curations from {file_out}.")
    with open(file_out) as f:
        curations_in = yaml.load(f, Loader=yaml.loader.SafeLoader)
    ids = [ a['id'] for a in curations_in ]

now = datetime.now()
for pkg in licenses:
    if pkg not in ids:
        new = {}
        new['id'] = pkg
        new['curations'] = {
            'comment': f'Curated from clearlydefined on {now}, see {urls[pkg]}',
            'concluded_license': f'{licenses[pkg]}'
        }
        curations_in.append(new)

print(f"Write curations to {file_out}.")
with open(file_out, 'w') as f:
    yaml.dump(curations_in, f, sort_keys=False) #, default_flow_style=False)



