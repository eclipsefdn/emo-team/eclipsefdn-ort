###########################################################################
# Copyright (c) 2022 The Eclipse Foundation.
#
# This program and the accompanying materials are made available under
# the terms of the Eclipse Public License 2.0 which accompanies this
# distribution, and is available at https://www.eclipse.org/legal/epl-2.0
#
# SPDX-License-Identifier: EPL-2.0
###########################################################################

import sys
import json


if len(sys.argv) == 3:
  proj = sys.argv[1]
  file = sys.argv[2]
else:
  exit("Usage: extract_false_positives.py project_name scancode_result.json")

# We want to provide the complete url to the files, so we need to store
# the urls of their project's repos.
urls = {
  'che': 'https://github.com/eclipse/che/blob/main/',
  'jgit': 'https://git.eclipse.org/c/jgit/jgit.git/tree/',
  'tycho': 'https://github.com/eclipse/tycho/blob/master/',
  'cdt': 'https://git.eclipse.org/c/cdt/org.eclipse.cdt.git/tree/',
}

url = ''
if proj in urls.keys():
  url = urls[proj]
else: 
  exit("Project {proj} is not recognised as a project, please edit the urls var.")


with open(file, 'r') as f:
  try:
    doc = json.load(f)
  except json.JSONDecodeError as e:
    print(f"Could not parse JSON from {file}: error {e}")

false_positives = []
for f in doc['files']:
#  print(f"* {f['path']}")
  has_unknown = False
  has_epl2 = False
  loc = None
  for l in f['licenses']:
#    print(f"  - {l['key']}")
    if l['key'] == 'unknown-license-reference':
      has_unknown = True
      loc = [l['start_line'], l['end_line']]
    elif l['key'] == 'epl-2.0':
      has_epl2 = True
  if has_unknown: # and has_epl2:
    false_positives.append((f['path'], loc))

print('file_url,start_line,end_line')
[ print(f'{url}{x[0]},{x[1][0]},{x[1][1]}') for x in false_positives ]


