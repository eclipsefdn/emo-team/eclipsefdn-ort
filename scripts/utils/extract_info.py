###########################################################################
# Copyright (c) 2022 The Eclipse Foundation.
#
# This program and the accompanying materials are made available under
# the terms of the Eclipse Public License 2.0 which accompanies this
# distribution, and is available at https://www.eclipse.org/legal/epl-2.0
#
# SPDX-License-Identifier: EPL-2.0
###########################################################################

import sys
import json
import yaml
import re
import os
import getopt
import pprint
import csv
from datetime import datetime
from pathlib import Path
from typing import Any, Dict, List


# Parse command line arguments
try:
    arguments, values = getopt.getopt(sys.argv[1:], "r:p:o:", ["results=", "project=", "output="])
except getopt.error as err:
    # Output error, and return with an error code
    print(str(err))
    sys.exit(2)

# Define defaults for vars
dir_results = "/data/results/"
dir_output = None 
project = ""

# Evaluate given options
for current_argument, current_value in arguments:
    if current_argument in ("-r", "--results"):
        dir_results = current_value
        dir_results_path = Path(dir_results)
    elif current_argument in ("-p", "--project"):
        project = current_value
    elif current_argument in ("-o", "--output"):
        dir_output = current_value
        dir_output_path = Path(dir_output)

    
#if len(sys.argv) > 1:
#    dir_results = sys.argv[1]
#    dir_results_path = Path(dir_results)
#else:
#    exit("Usage: extract_info.py /dir/results/")

date = datetime.now()
print(f"\n# Executing extraction script on {date}.\n")

try:
    if dir_results_path and dir_results_path.is_dir(): 
        print(f"- Reading results from {dir_results}.")
    else:
        exit(f"Cannot find results dir {dir_results}. Exiting.")

    if dir_output_path and dir_output_path.is_dir(): 
        print(f"- Publishing analysis to {dir_output}.")
    else:
        exit(f"Cannot find output dir {dir_output}. Exiting.")
except NameError:
    print("Usage: script.py -r results_dir -o output_dir.")
    exit()


########################
# Utility functions
########################

def convert_violations_to_csv(violations):
    csv = ["rule,package,license,license_source,severity,message"]
    for v in violations:
        line = f"{v['rule']},{v['pkg']},{v['license']},{v['license_source']},{v['severity']},{v['message']}"
        csv.append(line)
    return csv

def publish_run(time: datetime, project: str, repo: str, dir_pub: str, run: Dict[str, Any]):

    # Create run directory if it doesn't exist.
    date_path = datetime.strftime(time, "%Y%m%d%H%M")
    path = f"{project}_{repo}_{date_path}"
    dir_run = os.path.join(dir_pub, "runs", path)
    dir_files = f"/files/{project}/{repo}"
    try:
        os.makedirs(dir_run, exist_ok = True)
    except OSError as error:
        print("Directory '%s' can not be created" % dir_run)

    #date = datetime.strftime(time, "%Y-%m-%d")
    date = time.isoformat()
    header = (
    "---\n"
    f"title: Run {project} - {repo} - {date_path}\n"
    f"date: {date}\n"
#    "type: single"
    "---\n\n"
    )
    print(f"  - Found repo [{repo}] in {dir_run} run {time}.")
    content = [header]
    content.append("## Summary\n\n")
    content.append(f"- Project: [{project}](/projects/{project}).\n")
    content.append(f"- Repository: [{repo}](/projects/{project}#{repo}).\n")
    content.append(f"- Date: {date}.\n")
    file_out = os.path.join(dir_run, f"{project}_{repo}_{date_path}_violations.csv")
    file_csv = f"/runs/{project}_{repo}_{date_path}/{project}_{repo}_{date_path}_violations.csv"
    with open(file_out,'w') as f:
        f.write('\n'.join(convert_violations_to_csv(run["evaluation"]["violations"])))
    content.append(f"- Violations: {len(run['evaluation']['violations'])} -- [Download CSV]({file_csv}).\n")
    if run['evaluation']['v_proj_lic'] != 0:
        content.append(f"  - PROJECT_LICENSE_CHECK {run['evaluation']['v_proj_lic']}\n")
    if run['evaluation']['v_dep_lic'] != 0:
        content.append(f"  - DEPENDENCY_LICENSE_CHECK {run['evaluation']['v_dep_lic']}\n")
    if run['evaluation']['v_osadl_comp'] != 0:
        content.append(f"  - OSADL_PROJECT_LICENSE_COMPATIBILITY {run['evaluation']['v_osadl_comp']}.\n")
    content.append(f"- Log file: [{path}.log](/files/{project}/{repo}/{path}.log).\n")
    content.append(f"- Evaluation result file: [JSON report]({dir_files}/evaluation-result.json)\n")

    content.append("\n----\n\n")
    content.append("\n## Reports\n\n")
    content.append(f"List of ORT published reports:\n")
    content.append(f"- [WebApp report]({dir_files}/scan-report-web-app.html)\n")
    content.append(f"- [Static html report]({dir_files}/scan-report.html)\n")
    content.append(f"- [Notice file]({dir_files}/NOTICE_default)\n")
    content.append(f"- [SPDX SBOM]({dir_files}/bom.spdx.yml)\n")
    content.append(f"- [CycloneDX SBOM]({dir_files}/bom.cyclonedx.xml)\n")
    content.append("\n----\n\n")

    file_out = os.path.join(dir_run, "index.md") #f"{name}.md")
    with open(file_out,'w') as f:
        f.write(''.join(content))

        
def publish_project(name: str, dir_pub: str, project: List[Any]):

    # Create project directory if it doesn't exist.
    dir_project = os.path.join(dir_pub, "projects", name)
    try:
        os.makedirs(dir_project, exist_ok = True)
    except OSError as error:
        print("Directory '%s' can not be created" % dir_project)

    last_repo = None
    last_date = datetime(2000, 1, 1, 0, 0)
    for repo in project:
#        pprint.pprint(repo, project[repo])
        date = project[repo][-1]["time"]
        if date > last_date:
            last_repo = repo
            last_date = date
    last_run_date_path = datetime.strftime(project[last_repo][-1]["time"], "%Y%m%d%H%M")
    last_run_date = project[last_repo][-1]["time"].isoformat()

    header = (
    "---\n"
    f"title: Project {name}\n"
    f"date: {last_run_date}\n"
    "type: post\n"
    "---\n\n"
    )
    content = [header]
    #content.append('\n\n{{< alert "Message" "info" >}}\n\n')

    content.append("## Project Summary\n\n")
    content.append(f"* Project ID: `{name}`\n")
    content.append(f"* Project PMI: https://projects.eclipse.org/projects/{name}\n")
    content.append(f"* Last analysis on `{last_run_date_path}`\n")
    content.append("\n\n")
    
    for repo in project:
        last_run = project[repo][-1]
        date = last_run["time"].isoformat()
        last_run_date = last_run["time"].isoformat()
        last_run_date_path = datetime.strftime(last_run["time"], "%Y%m%d%H%M")
        last_run_path_rel = f"{name}_{repo}_{last_run_date_path}"
        last_run_path_files = f"{name}/{repo}"
        content.append("\n\n----\n\n")
        content.append(f"### {repo}\n\n")
        content.append("**Summary**\n\n")
        content.append(f"- Date of last run: [{date}](/runs/{last_run_path_rel}/)\n")
        if "evaluation" in last_run:
            run_path = f"/runs/{last_run_path_rel}"
            violations_path = f"{run_path}/{name}_{repo}_{last_run_date_path}_violations.csv"
            content.append(f"- Violations: {len(last_run['evaluation']['violations'])} -- [Download CSV]({violations_path}).\n")
            if run['evaluation']['v_proj_lic'] != 0:
                content.append(f"  - PROJECT_LICENSE_CHECK {run['evaluation']['v_proj_lic']}\n")
            if run['evaluation']['v_dep_lic'] != 0:
                content.append(f"  - DEPENDENCY_LICENSE_CHECK {run['evaluation']['v_dep_lic']}\n")
            if run['evaluation']['v_osadl_comp'] != 0:
                content.append(f"  - OSADL_PROJECT_LICENSE_COMPATIBILITY {run['evaluation']['v_osadl_comp']}.\n")
            content.append(f"- Reports:\n")
            content.append(f"  - [WebApp report](/files/{last_run_path_files}/scan-report-web-app.html)\n")
            content.append(f"  - [Static html report](/files/{last_run_path_files}/scan-report.html)\n")
            content.append(f"  - [Notice file](/files/{last_run_path_files}/NOTICE_default)\n")
            content.append(f"  - [SPDX SBOM](/files/{last_run_path_files}/bom.spdx.yml)\n")
            content.append(f"  - [CycloneDX SBOM](/files/{last_run_path_files}/bom.cyclonedx.xml)\n")
#        content.append(f"This is the list of runs for repository {repo}:\n")
#        for run in reversed(project[repo]):
#            time = run["time"]
#            date_path = datetime.strftime(time, "%Y%m%d%H%M")
#            path = f"{name}_{repo}_{date_path}"
#            dir_run = os.path.join(dir_pub, "runs", path)
#            if "evaluation" in run:
#                count = f" ({len(run['evaluation']['violations'])} violations)"
#            else:
#                count = ""
#            content.append(f"- [{time}](/runs/{path}) {count}\n")
        publish_run(time=project[repo][-1]['time'], project=name, repo=repo, dir_pub=dir_pub, run=last_run)
    file_out = os.path.join(dir_project, "index.md") #f"{name}.md")
    with open(file_out,'w') as f:
        f.write(''.join(content))
        

def publish_analysis(dir_pub: str, projects: Dict[str, Any]) -> None:
    print(f"\n# Publishing to [{dir_pub}].\n")
    for project in projects.keys():
        print(f'- Project [{project}]].')
        publish_project(project, dir_pub, projects[project])


def analyze_run_evaluation(log: str) -> Dict[str, Any]:
    ret = {}
    with open(log, 'r') as f:
        try:
            doc = json.load(f)
        except json.JSONDecodeError as e:
            print(f"Could not parse JSON from {log}: error {e}")
            return None
    
    # Record licenses
    ret['licenses'] = {}
    for package in doc['scanner']['scan_results']:
        if not package.startswith('Unmanaged:'):
            continue
        for subpackage in doc['scanner']['scan_results'][package]:
            if 'summary' in subpackage and 'licenses' in subpackage['summary']:
                for license in subpackage['summary']['licenses']:
                    if license["license"] not in ret['licenses'].keys():
                        ret['licenses'][license['license']] = {}
                    if package not in ret['licenses'][license['license']]:
                        ret['licenses'][license['license']][package] = []
                    ret['licenses'][license['license']][package].append(license['location'])

    # Record violations
    ret["violations"] = doc['evaluator']['violations']
    ret['v_proj_lic'] = 0
    ret['v_dep_lic'] = 0
    ret['v_osadl_comp'] = 0 
    for violation in doc['evaluator']['violations']:
        if violation['rule'] == 'PROJECT_LICENSE_CHECK':
            ret['v_proj_lic'] += 1 
        if violation['rule'] == 'DEPENDENCY_LICENSE_CHECK':
            ret['v_dep_lic'] += 1 
        if violation['rule'] == 'OSADL_PROJECT_LICENSE_COMPATIBILITY':
            ret['v_osadl_comp'] += 1
 #       ret['v_project_license_check']++ if violation['rule'] == PROJECT_LICENSE_CHECK
    print(f"  Evaluation: Found {len(ret['violations'])} violations.")
    return ret

########################
# Main
########################

########################
# Parse results files
########################

re_evaluation_pattern = re.compile("^(?P<project>[^_]+)_(?P<repo>[^_]+)_(?P<time>\d+).log$")

# Initialise counting vars
nb_projs = 0
nb_repos = 0
nb_runs = 0

# Initialise var to store final results
projects = {}

for (dirpath, dirnames, filenames) in os.walk(dir_results):
    # Identify run directories and split them in project / repo / run_time.
    if ('evaluation-result.json' in filenames):
        times = []
        project = ""
        repo = ""
        for filelog in filenames:
            re_evaluation_match = re_evaluation_pattern.match(filelog)
#            print(f"- Analysing Project [{filelog}] in [{dirpath}].")
            if re_evaluation_match is not None:
                project = re_evaluation_match.group('project')
                repo = re_evaluation_match.group('repo')
                time = re_evaluation_match.group('time')
                times.append(time)
                print(f"- Found Project [{project}] Repo [{repo}] Run [{time}].")
                # run = {
                #   "time": datetime,
                #   "evaluation": [
                #     "licenses": []
                #     "violations": []
                #   ]
                # }
        if (len(times) > 0): 
            # Add project to list if not already there
            if project not in projects:
                projects[project] = {}
                nb_projs += 1
            # Add repo to project if not already there
            if repo not in projects[project]:
                projects[project][repo] = []
                nb_repos += 1
            run = {}
            run["time"] = datetime.strptime(times[0], "%Y%m%d%H%M")
            # Analyse evaluation results and retrieve info
            log = os.path.join(dirpath, "evaluation-result.json")
            run["evaluation"] = analyze_run_evaluation(log)
            projects[project][repo].append(run)
            nb_runs += 1
            print(f"  Stored Project [{project}] Repo [{repo}] Run [{time}].")
            
        
########################
# Publish analysis
########################

print(f"Statistics: found {nb_projs} projects, {nb_repos} repositories, {nb_runs} runs.")

if dir_output:
    #pprint.pprint(projects)
    publish_analysis(dir_pub=dir_output, projects=projects)

print("# Writing violations.csv file.")
    
# Store errors in big CSV file.
violations = [["project", "repository", "run", "rule", "package",
               "license", "license_source", "severity", "message"]]
errors = dict()
for project in projects:
    for repo in projects[project]:
        # Get last run for this project
        run = projects[project][repo][-1]["time"]
        for v in projects[project][repo][-1]["evaluation"]["violations"]:
            # Prepare CSV
            line = [project, repo, run, v['rule'], v['pkg'],
                    v['license'], v['license_source'],
                    v['severity'], v['message']]
            violations.append(line)
            # Prepare markdown export
            v["project"] = project
            v["repo"] = repo
            v["run"] = run
            errors.setdefault(v['rule'], []).append(v)

# Convert violations to csv and spit it out.
file_out = os.path.join(dir_output, f"violations.csv")
with open(file_out,'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(violations)

# Write errors md index file.
for rule in errors.keys():
    error = errors[rule]
    header = (
    "---\n"
    f"title: Error {rule} ({len(error)})\n"
    "type: post\n"
        f"weight: -{len(error)}\n"
    "---\n\n"
    )
    content = [header]    
    content.append(f"Error {rule} encountered {len(error)} times across all projects.")
    for v in error:
        date_path = datetime.strftime(v['run'], "%Y%m%d%H%M")
#        content.append(f"- {v}\n")
        content.append(f"- [{v['project']} - {v['repo']}](/projects/{v['project']}) - [{v['run']}](/runs/{v['project']}_{v['repo']}_{date_path}) - {v['pkg']} - {v['license']}\n")
    # Create errors directory if it doesn't exist.
    dir_error = os.path.join(dir_output, "errors", rule)
    try:
        os.makedirs(dir_error, exist_ok = True)
    except OSError as error:
        print("Directory '%s' can not be created" % dir_error)

    file_out = os.path.join(dir_error, f"index.md")
    with open(file_out,'w') as f:
        f.write('\n'.join(content))
    

print("# Extraction finished.")

