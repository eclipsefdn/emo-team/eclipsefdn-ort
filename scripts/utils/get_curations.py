###########################################################################
# Copyright (c) 2022 The Eclipse Foundation.
#
# This program and the accompanying materials are made available under
# the terms of the Eclipse Public License 2.0 which accompanies this
# distribution, and is available at https://www.eclipse.org/legal/epl-2.0
#
# SPDX-License-Identifier: EPL-2.0
###########################################################################

# This script builds an ORT curations file from the Eclipse curations file.

import sys
import yaml

# Read in file name from command line.
file_in = sys.argv[1]
print(f"Using file {file_in}.")

with open(file_in, 'r') as f:
    try:
        doc = yaml.safe_load(f)
    except yaml.YAMLError as e:
        print(f"Could not parse YAML from {ort_license}: error {e}")
        exit()

out = []
for c in doc:
    lic = c['curations']['concluded_license']
    # Remove empty concluded_license strings
    if lic != "":
        # Remove SPDX expressions
        if ('AND' not in lic) and ('OR' not in lic):
            out.append(f'- id: "{c["id"]}"')
            out.append(f'  curations:')
            out.append(f'    concluded_license: "{c["curations"]["concluded_license"]}"')

# Write the result to file 'curations.yml'.
out_s = "\n".join(out)
with open('curations.yml', 'w') as file:
        file.write(out_s)
